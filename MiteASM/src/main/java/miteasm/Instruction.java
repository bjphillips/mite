/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

/**
 * Internal representation of a parsed instruction.
 */
public class Instruction {
	private int address = 0;
	private Operation operation;
	private Register rs = Register.R0;
	private Register rt = Register.R0;
	private Register rd = Register.R0;
	private int immediate = 0;
	private String label;
	private String immediateLabel;
	private String sourceLine;

	public String getSourceLine() {
		return sourceLine;
	}

	public void setSourceLine(String sourceLine) {
		this.sourceLine = sourceLine;
	}

	public String getImmediateLabel() {
		return immediateLabel;
	}

	public void setImmediateLabel(String immediateLabel) {
		this.immediateLabel = immediateLabel;
	}
	
	public Operation getOperation() {
		return operation;
	}

	public Register getRs() {
                return rs;
	}

	public Register getRt() {
		return rt;
	}

	public Register getRd() {
		return rd;
	}

	public int getShamt() {
		return immediate & 0x1F;    // just mask out the lower 5 bits
	}

	public String getLabel() {
		return label;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public void setRs(Register rs) {
		this.rs = rs;
	}

	public void setRt(Register rt) {
		this.rt = rt;
	}

	public void setRd(Register rd) {
		this.rd = rd;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getImmediate() {
		return immediate;
	}

	public void setImmediate(int immediate) {
		this.immediate = immediate;
	}

	public int getAddress() {
		return address;
	}
	
}
