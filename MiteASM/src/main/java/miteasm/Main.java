/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

public class Main {

	/**
	 * This is a utility class that is not meant to be instantiated.
	 * In such cases it is good practice to create a private constructor to
	 * hide the public one that Java would otherwise create implicitly.
	 * Refer squid:S1118.
	 */
	private Main() {
	}

	/**
	 * Main application entry point.
	 * @param args
	 * @throws java.lang.Exception
	 */
	public static void main(String[] args) throws Exception {
		MiteAssembler miteAssembler = new MiteAssembler();
		miteAssembler.runController();
	}
}
