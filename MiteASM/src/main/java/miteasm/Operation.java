/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

import static miteasm.OperationType.*;

/**
 * The machine operations supported by the Mite processor along with
 * fields used for their encoding.
 */
public enum Operation {
	ADD (RTYPE, 0x00, 0x20, "add"), 
	SUB (RTYPE, 0x00, 0x22, "sub"), 
	AND (RTYPE, 0x00, 0x24, "and"), 
	OR (RTYPE, 0x00, 0x25, "or"), 
	SLT (RTYPE, 0x00, 0x2A, "slt"), 
	SLL (RTYPE, 0x00, 0x00, "sll"), 
	SRL (RTYPE, 0x00, 0x02, "srl"), 
	LW (ITYPE, 0x23, 0x00, "lw"), 
	SW (ITYPE, 0x2B, 0x00, "sw"), 
	BEQ (ITYPE, 0x04, 0x00, "beq"), 
	ADDI (ITYPE, 0x08, 0x00, "addi"), 
	J (JTYPE, 0x02, 0x00, "j"), 
	JAL (JTYPE, 0x03, 0x00, "jal"), 
	JR (RTYPE, 0x00, 0x08, "jr");
	
	private final OperationType type;
	private final int opcode;
	private final int funct;
	private final String mnemonic;
	
	Operation(OperationType type, int opcode, int funct, String mnemonic) {
		this.type = type;
		this.opcode = opcode;
		this.funct = funct;
		this.mnemonic = mnemonic;
	}
	
	OperationType getType() {
		return this.type;
	}
	
	int getOpcode() {
		return this.opcode;
	}
	
	int getFunct() {
		return this.funct;
	}
	
	String mnemonic() {
		return this.mnemonic;
	}
}
