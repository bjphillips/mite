/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

import java.util.*;
import miteassembly.*;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * Convert the syntax tree output from the ANTLR generated parser
 * into a list of instructions ready for assembling; also provides a helper
 * function to assemble an instruction.
 *
 * Syntax tree conversion is done using the *listener* pattern. ANTLR provides a tree
 * walker that calls our listener (or event handler) methods for each
 * phrase in the source code.
 */
class ParserOutputConverter extends MiteAssemblyBaseListener {
    private final List<Instruction> instructions;
	private Instruction currentInstruction;
	private boolean conversionErrorsFound;
	private final StringBuilder logMessages;
	private int currentAddress;
	private final Map<String, Integer> labelMap;
	
	public ParserOutputConverter() {
		instructions = new ArrayList<>();
		currentInstruction = null;
		conversionErrorsFound = false;
		logMessages = new StringBuilder();
		currentAddress = 0;
		labelMap = new HashMap<>();
	}

	public List<Instruction> getInstructions() {
		return instructions;
	}

	public boolean conversionErrorsFound() {
		return this.conversionErrorsFound;
	}
	
	public String getLogMessages() {
		return logMessages.toString();
	}
	
	/**
	 * For each line in the assembly program (for each labelledInstruction)
	 * begin a new currentInstruction and begin populating its properties.
	 */
	@Override 
	public void enterLabelledInstruction(MiteAssemblyParser.LabelledInstructionContext ctx) {
		currentInstruction = new Instruction();
		currentInstruction.setAddress(currentAddress);
		if (ctx.label() != null) {
			String label = ctx.label().getText();
			label = label.substring(0, label.length()-1);
			currentInstruction.setLabel(label);
			if (labelMap.containsKey(label)) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Duplicate label found: ").append(label).append("\n");
				conversionErrorsFound = true;
			}
			labelMap.put(label, currentAddress);
		}
		currentAddress += 4;
	}

	/** 
	 * Extract the properties for register-register instructions. For example:
	 * add  $rd, $rs, $rt
     * ('add' | 'sub' | 'and' | 'or' | 'slt') Register ',' Register ',' Register
	 * @param ctx 
	 */
	@Override 
	public void exitRrinstruction(MiteAssemblyParser.RrinstructionContext ctx) { 
		String mnemonic = ctx.getStart().getText().toUpperCase();
		currentInstruction.setOperation(Operation.valueOf(mnemonic));
		currentInstruction.setRd(Register.fromName(ctx.Register(0).getText()));
		currentInstruction.setRs(Register.fromName(ctx.Register(1).getText()));
		currentInstruction.setRt(Register.fromName(ctx.Register(2).getText()));
	}

	/** 
	 * Extract the properties for register-immediate instructions. For example:
	 * sll  $rd, $rt, shamt   ; RTYPE
	 * addi $rt, $rs, SignImm ; ITYPE
	 * beq $rs, $rt, BTA      ; ITYPE
	 * ('sll' | 'srl' | 'beq' | 'addi') Register ',' Register ',' (HexInteger | DecInteger | Identifier)
	 */
	@Override 
	public void exitRiinstruction(MiteAssemblyParser.RiinstructionContext ctx) { 
		String mnemonic = ctx.getStart().getText().toUpperCase();
		currentInstruction.setOperation(Operation.valueOf(mnemonic));
		if (currentInstruction.getOperation().getType()==OperationType.ITYPE) {
			if (currentInstruction.getOperation()==Operation.BEQ) {
				currentInstruction.setRs(Register.fromName(ctx.Register(0).getText()));
				currentInstruction.setRt(Register.fromName(ctx.Register(1).getText()));				
			}
			else {
				currentInstruction.setRt(Register.fromName(ctx.Register(0).getText()));
				currentInstruction.setRs(Register.fromName(ctx.Register(1).getText()));
			}
		}
		else { // RTYPE
			currentInstruction.setRd(Register.fromName(ctx.Register(0).getText()));
			currentInstruction.setRt(Register.fromName(ctx.Register(1).getText()));
		}
		if (ctx.HexInteger() != null) {
                        String hexString;
                        int sign;
                        if ("-".equals(ctx.HexInteger().getText().substring(0,1))) {
                                hexString = new String(ctx.HexInteger().getText().substring(3));
                                sign = -1;
                        }
                        else {
                                hexString = new String(ctx.HexInteger().getText().substring(2));
                                sign = 1;
                        }                        
			try {
                                currentInstruction.setImmediate(sign * Integer.parseInt(hexString,16));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad hex integer format: ").append(ctx.HexInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}
		}
                else if (ctx.DecInteger() != null) {
			try {
				currentInstruction.setImmediate(Integer.parseInt(ctx.DecInteger().getText(),10));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad decimal integer format: ").append(ctx.DecInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}                    
                }
		else {
			currentInstruction.setImmediateLabel(ctx.Identifier().getText());
		}
	}
		
	/** 
	 * Extract the properties for register instructions. For example:
	 * jr   $rs
	 * 'jr' Register
	 */
	@Override 
	public void exitRinstruction(MiteAssemblyParser.RinstructionContext ctx) { 
		String mnemonic = ctx.getStart().getText().toUpperCase();
		currentInstruction.setOperation(Operation.valueOf(mnemonic));
		currentInstruction.setRs(Register.fromName(ctx.Register().getText()));
	}

	/** 
	 * Extract the properties for register-offset instructions. For example:
	 * sw   $rt, SignImm($rs)
	 * ('sw' | 'lw') Register ',' (HexInteger | DecInteger)'('Register')'
	 */
	@Override 
	public void exitRoinstruction(MiteAssemblyParser.RoinstructionContext ctx) { 
		String mnemonic = ctx.getStart().getText().toUpperCase();
		currentInstruction.setOperation(Operation.valueOf(mnemonic));
		currentInstruction.setRt(Register.fromName(ctx.Register(0).getText()));
		currentInstruction.setRs(Register.fromName(ctx.Register(1).getText()));
		if (ctx.HexInteger() != null) {
			try {
				currentInstruction.setImmediate(Integer.parseInt(ctx.HexInteger().getText().substring(2),16));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad hex integer format: ").append(ctx.HexInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}
		}
                else if (ctx.DecInteger() != null) {
			try {
				currentInstruction.setImmediate(Integer.parseInt(ctx.DecInteger().getText(),10));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad decimal integer format: ").append(ctx.DecInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}
		}
	}

	/** 
	 * Extract the properties for immediate instructions. For example:
	 * j    JTA
     * ('j' | 'jal') (HexInteger | DecInteger | Identifier)
	 */
	@Override 
	public void exitIinstruction(MiteAssemblyParser.IinstructionContext ctx) { 
		String mnemonic = ctx.getStart().getText().toUpperCase();
		currentInstruction.setOperation(Operation.valueOf(mnemonic));
		if (ctx.HexInteger() != null) {
			try {
				currentInstruction.setImmediate(Integer.parseUnsignedInt(ctx.HexInteger().getText().substring(2),16));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad unsigned hex integer format: ").append(ctx.HexInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}
		}
                else if (ctx.DecInteger() != null) {
			try {
				currentInstruction.setImmediate(Integer.parseUnsignedInt(ctx.DecInteger().getText(),10));
			} 
			catch (NumberFormatException ex) {
				logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Bad unsigned decimal integer format: ").append(ctx.DecInteger().getText()).append("\n");
				conversionErrorsFound = true;				
			}
		}
		else {
			currentInstruction.setImmediate(-1);
			currentInstruction.setImmediateLabel(ctx.Identifier().getText());
		}
	}

	/** 
	 * On exiting a line, add the currentInstruction to the list of all instructions.
	 */
	@Override 
	public void exitLabelledInstruction(MiteAssemblyParser.LabelledInstructionContext ctx) {
		// The following line excludes all the whitespace. My solution
		// has been to use the visitTerminal method below to build the source
		// line token by token.
		//currentInstruction.setSourceLine(ctx.getText());
		instructions.add(currentInstruction);
	}

	/** 
	 * Add each terminal in the source line to the sourceLine property in the currentInstruction
	 * so that we can include it as a comment in the assembler output.
	 */
	@Override 
	public void visitTerminal(TerminalNode node) {
		String sourceLine = currentInstruction.getSourceLine();
		if (sourceLine != null) {
			sourceLine = sourceLine + node.getText() + " ";
		}
		else {
			sourceLine = node.getText() + " ";
		}
		currentInstruction.setSourceLine(sourceLine.toString());
	}

	/** 
	 * Produce the assembler output when we finish walking through the source file.
	 */
	@Override 
	public void exitSource(MiteAssemblyParser.SourceContext ctx) { 
		// For instructions with labels for immediates, set the immediate value
		// to the value of the label.
		for (Instruction instruction: instructions) {
			String label = instruction.getImmediateLabel();
			if (label != null) {
				if (!labelMap.containsKey(label)) {
					// an immediate refers to a label that does not exist
					logMessages.append("line ").append(ctx.getStart().getLine()).append(":").append(ctx.getStart().getCharPositionInLine()).append(" Undefined label: ").append(label).append("\n");
					conversionErrorsFound = true;				
				}
				else {
					instruction.setImmediate(labelMap.get(label));
				}
			}
		}
		if (conversionErrorsFound()) {
			return;
		}
		// assemble
		logMessages.append("module InstructionMemory(input logic [5:0] A, output logic [31:0] RD);\n");
		logMessages.append("   always_comb\n");
		logMessages.append("      case (A<<2)\n");
		for (Instruction instruction: instructions) {
			logMessages.append("         ").append(assemble(instruction));
		}
		logMessages.append("         default: RD = 32'h00000000;\n");
		logMessages.append("      endcase\n");
		logMessages.append("endmodule\n");
	}
	
	/**
	 * A helper function to assemble a single instruction.
	 * @param instruction
	 * @return 
	 */
	private static String assemble(Instruction instruction) {
		StringBuilder line = new StringBuilder();
		int machineInstruction = instruction.getOperation().getOpcode()<<26;
		switch (instruction.getOperation().getType()) {
			case RTYPE:
				machineInstruction |= instruction.getRs().getNumber()<<21;
				machineInstruction |= instruction.getRt().getNumber()<<16;
				machineInstruction |= instruction.getRd().getNumber()<<11;
				machineInstruction |= instruction.getShamt()<<6;
				machineInstruction |= instruction.getOperation().getFunct();
				break;
			case ITYPE:
				machineInstruction |= instruction.getRs().getNumber()<<21;
				machineInstruction |= instruction.getRt().getNumber()<<16;
				if (instruction.getOperation()==Operation.BEQ) {
					// BTA = PC + 4 + (SignImm << 2)
					// SignImm = (BTA - PC - 4)>>2
					int SignImm = (instruction.getImmediate()-instruction.getAddress()-4)>>2;
					machineInstruction |= SignImm & 0xFFFF;
				}
				else {
					machineInstruction |= instruction.getImmediate() & 0xFFFF;
				}
				break;
			case JTYPE:
				// JTA = {(PC + 4)[31:28], addr, 2’b0}
				machineInstruction |= (instruction.getImmediate()>>2) & 0x3FFFFFF;
				break;
		}
		line.append("8'h").append(Integer.toHexString(instruction.getAddress()));
		line.append(": RD = 32'h");
		line.append(Integer.toHexString(machineInstruction));
		line.append("; // ").append(instruction.getSourceLine()).append("\n");
		return line.toString();
	}
}

