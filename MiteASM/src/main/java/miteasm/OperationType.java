/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

/**
 * The four operation types supported by the Mite processor.
 */
public enum OperationType {
    RTYPE, ITYPE, JTYPE, FTYPE
}
