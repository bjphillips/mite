/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

/**
 * The 32 registers supported by the Mite processor along with encoding 
 * details.
 */
public enum Register {
	R0 (0x00, "$0"),
	AT (0x01, "$at"),
	V0 (0x02, "$v0"),
	V1 (0x03, "$v1"),
	A0 (0x04, "$a0"),
	A1 (0x05, "$a1"),
	A2 (0x06, "$a2"),
	A3 (0x07, "$a3"),
	T0 (0x08, "$t0"),
	T1 (0x09, "$t1"),
	T2 (0x0A, "$t2"),
	T3 (0x0B, "$t3"),
	T4 (0x0C, "$t4"),
	T5 (0x0D, "$t5"),
	T6 (0x0E, "$t6"),
	T7 (0x0F, "$t7"),
	S0 (0x10, "$s0"),
	S1 (0x11, "$s1"),
	S2 (0x12, "$s2"),
	S3 (0x13, "$s3"),
	S4 (0x14, "$s4"),
	S5 (0x15, "$s5"),
	S6 (0x16, "$s6"),
	S7 (0x17, "$s7"),
	T8 (0x18, "$t8"),
	T9 (0x19, "$t9"),
	K0 (0x1A, "$k0"),
	K1 (0x1B, "$k1"),
	GP (0x1C, "$gp"),
	SP (0x1D, "$sp"),
	FP (0x1E, "$fp"),
	RA (0x1F, "$ra");
	
	private final int number;
	private final String name;
	
	Register(int number, String name) {
		this.number = number;
		this.name = name;
	}
	
	int getNumber() {
		return this.number;
	}
	
	String getName() {
		return this.name;
	}
		
	static Register fromName(String name) {
		for (Register register: Register.values()) {
			if (name.toLowerCase().equals(register.name)) {
				return register;
			}
		}
		return null;
	}
}
