/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import miteassembly.MiteAssemblyLexer;
import miteassembly.MiteAssemblyParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * FXML Controller class
 *
 */
public class MainWindowController implements Initializable {

    @FXML
    private TextArea logWindow;
    @FXML
    private Button openButton;

    private File initialDirectory;
    
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    /**
     * When the user clicks on the openButton, open an assembly language file
     * and assemble it.
     *
     * @param event
     */
    @FXML
    private void openPressed(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Assembly File");
        if (initialDirectory != null) {
            fileChooser.setInitialDirectory(initialDirectory);
        }
        File file = fileChooser.showOpenDialog(openButton.getScene().getWindow());

        // check if cancel was pressed
        if (file == null) {
            return;
        }
        
        // use the same dirctory as the starting point next time Open is pressed
        initialDirectory = file.getParentFile();
        
        // clear the low window
        logWindow.setText("");

        ANTLRInputStream input = null;
        try {
            // open the source file as a character stream
            input = new ANTLRFileStream(file.getAbsolutePath());
        } catch (Exception e) {
            appendLogWindow("Unable to open the assembler source file: " + file.getName() + "\n");
            return;
        }

        // create a lexer that feeds off of input CharStream
        MiteAssemblyLexer lexer = new MiteAssemblyLexer(input);

        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // create a parser that feeds off the tokens buffer
        MiteAssemblyParser parser = new MiteAssemblyParser(tokens);

        // add an error listener to the parser
        parser.removeErrorListeners(); // remove ConsoleErrorListener 
        ErrorFlaggingListener listener = new ErrorFlaggingListener();
        parser.addErrorListener(listener);

        // parse the source
        ParseTree tree = parser.source(); // begin parsing at source rule
        if (listener.parseErrorsFound) {
            appendLogWindow("Parse errors found.\n");
            return;
        }

        // create a generic parse tree walker that can trigger callbacks
        ParseTreeWalker walker = new ParseTreeWalker();

        // create a parser output converter to create a list of productions as it listens during the walk
        ParserOutputConverter converter = new ParserOutputConverter();

        // walk the tree created during the parse, trigger callbacks to the converter
        walker.walk(converter, tree);
        appendLogWindow(converter.getLogMessages());
        if (converter.conversionErrorsFound()) {
            appendLogWindow("Code conversion errors found.\n");
            return;
        }

    }

    /**
     * A local helper function to add text to the logWindow and remove old text
     * so that it does not grow to absurd lengths.
     */
    private void appendLogWindow(String s) {
        StringBuilder l = new StringBuilder(logWindow.getText()).append(s);
        if (l.length() > 5000) {
            l.delete(0, l.length() - 5001);
        }
        logWindow.setText(l.toString());
        logWindow.appendText(""); // force a scroll to the bottom
    }

    /**
     * Listen to errors during the parse and report them to the log window.
     */
    private class ErrorFlaggingListener extends BaseErrorListener {

        boolean parseErrorsFound;

        ErrorFlaggingListener() {
            parseErrorsFound = false;
        }

        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
                int line, int charPositionInLine, String msg, RecognitionException e) {
            appendLogWindow("line " + line + ":" + charPositionInLine + " " + msg + "\n");
            parseErrorsFound = true;
        }
    }
}
