/* 
 * Copyright (c) 2015, Braden Phillips, The University of Adelaide.
 */
package miteasm;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * Invoke the JavaFX GUI under swing.
 */
public class MiteAssembler {

	/**
	 * Call this method from the swing application thread to launch the
	 * JavaFX GUI.
	 */
	void runController() {
		// Create the EDT (Event Dispatch Thread)
		Runnable edtRunnable = new Runnable() {
			@Override
			public void run() {
				initAndShowGUI();
			}
		};
		SwingUtilities.invokeLater(edtRunnable);
	}

	private void initAndShowGUI() {
		// This method is invoked on the EDT (Event Dispatch Thread)
		JFrame frame = new JFrame("MiteASM");
		final JFXPanel fxPanel = new JFXPanel();
		frame.add(fxPanel);
		frame.setSize(420, 320);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
			}
		});

		// Create the JavaFX thread
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				initFX(fxPanel);
			}
		});
	}

	private void initFX(JFXPanel fxPanel) {
		// This method is invoked on the JavaFX thread
		Scene scene = createScene();
		fxPanel.setScene(scene);
	}

	private Scene createScene() {
		Parent root = null;
		FXMLLoader loader;
		MainWindowController controller = null;
		try {
			loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
			root = loader.load();
			controller = loader.<MainWindowController>getController();
		} catch (Exception ex) {
			System.err.println("Exception loading MainWindow.fxml: " + ex.getMessage());
			// Unchecked exception justified as there no reasonable hope of recovering from this
			throw new RuntimeException(ex);
		}
		return new Scene(root);
	}

}
