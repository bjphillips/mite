grammar MiteAssembly;

// instructions supported:
// * add  $rd, $rs, $rt
// * sub  $rd, $rs, $rt
// * and  $rd, $rs, $rt
// * or   $rd, $rs, $rt
// * slt  $rd, $rs, $rt
// * sll  $rd, $rt, shamt
// * srl  $rd, $rt, shamt
// * lw   $rt, SignImm($rs)
// * sw   $rt, SignImm($rs)
// * beq  $rs, $rt, BTA
// * addi $rt, $rs, SignImm
// * j    JTA
// * jal  JTA
// * jr   $rs

source:
    labelledInstruction*
;

labelledInstruction:
    label? instruction
;

label:
    Identifier':'
;

instruction:
    rrinstruction  
    | riinstruction
    | rinstruction 
    | roinstruction
    | iinstruction 
;

// register-register instructions e.g.
// * add  $rd, $rs, $rt
rrinstruction:
    ('add' | 'ADD' | 'sub' |'SUB' | 'and' | 'AND' | 'or' | 'OR' | 'slt' | 'SLT') Register ',' Register ',' Register
; 

// register-immediate instructions e.g.
// * sll  $rd, $rt, shamt
riinstruction:
    ('sll' | 'SLL' | 'srl' | 'SRL' | 'beq' | 'BEQ' | 'addi' | 'ADDI') Register ',' Register ',' (HexInteger | DecInteger | Identifier)
; 

// register instructions e.g.
// * jr   $rs
rinstruction:
    ('jr' | 'JR') Register
; 

// register-offset instructions e.g.
// * sw   $rt, SignImm($rs)
roinstruction:
    ('sw' | 'SW' | 'lw' | 'LW') Register ',' (HexInteger | DecInteger)'('Register')'
; 

// immediate instructions e.g.
// * j    JTA
iinstruction:
    ('j' | 'J' |'jal' | 'JAL') (HexInteger | DecInteger | Identifier)
; 

/* L E X E R   R U L E S */

Register:
    '$0' |
    '$at' | '$AT' | 
    '$v''0'..'1' | '$V''0'..'1' |
    '$a''0'..'3' | '$A''0'..'3' |
    '$t''0'..'9' | '$T''0'..'9' |
    '$s''0'..'7' | '$S''0'..'7' |
    '$k''0'..'1' | '$K''0'..'1' |
    '$gp' | '$GP' | 
    '$sp' | '$SP' | 
    '$fp' | '$FP' |  
    '$ra' | '$RA' 
;

Identifier:
    ('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'_'|'0'..'9')*
;

DecInteger:
    '-'?('0'..'9')+
;

HexInteger:
    '-'?('0x'|'0X')('0'..'9'|'A'..'F'|'a'..'f')+
;

Whitespace:
    (' '|'\t'|'\r'|'\n')+ -> skip
;

LineComment:   
    '#' .*? '\r'? '\n' -> skip
;
