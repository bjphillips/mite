# This is not intended to do anything useful except provide a set of 
# test cases for the parser.

# Check if all the registers are recognized
        add     $t0, $t1, $0
        add     $at, $v0, $v1
        add     $a0, $a1, $a2
        add     $a3, $t2, $t3
        add     $t4, $t5, $t6
        add     $t7, $s0, $s1
        add     $s2, $s3, $s4
        add     $s5, $s6, $s7
        add     $t8, $t9, $k0
        add     $k1, $gp, $sp
        add     $fp, $ra, $0

# How about in upper case?
        ADD     $T0, $T1, $0
        ADD     $AT, $V0, $V1
        ADD     $A0, $A1, $A2
        ADD     $A3, $T2, $T3
        ADD     $T4, $T5, $T6
        ADD     $T7, $S0, $S1
        ADD     $S2, $S3, $S4
        ADD     $S5, $S6, $S7
        ADD     $T8, $T9, $K0
        ADD     $K1, $GP, $SP
        ADD     $FP, $RA, $0

loop:                               # Label on a line of its own
        j       loop2               # Jump forwards
loop2:  j       loop                # Jump backwards

        addi    $t0, $0, 0x5A02     # positive hex
        addi    $t0, $0, -0x5A02    # negative hex
        addi    $t0, $0, 23042      # positive decimal
        addi    $t0, $0, -23042     # negative decimal
        addi    $t0, $0, 01         # positive decimal with leading zero
        addi    $t0, $0, -01        # negative decimal with leading zero

        sll     $t0, $t1, 0x05
        sll     $t0, $t1, 5
        sll     $t0, $t1, -0x05     # these don't make sense
        sll     $t0, $t1, -5        # but should not crash the assembler

        sll     $t0, $t1, 0x05
        srl     $t0, $t1, 0x05
        jr      $t0
        add     $t0, $t1, $t2
        sub     $t0, $t1, $t2
        and     $t0, $t1, $t2
        or      $t0, $t1, $t2
        slt     $t0, $t1, $t2
        j       loop
        j       0x00
        jal     loop
        jal     0x00
        beq     $t0, $t1, loop
        beq     $t0, $t1, 0x00
        addi    $t0, $t1, 0x01
        lw      $t0, 0x04($t1)
        sw      $t0, 0x04($t1)

        SLL     $T0, $T1, 0X05
        SRL     $T0, $T1, 0X05
        JR      $T0
        ADD     $T0, $T1, $T2
        SUB     $T0, $T1, $T2
        AND     $T0, $T1, $T2
        OR      $T0, $T1, $T2
        SLT     $T0, $T1, $T2
        J       loop
        J       0X00
        JAL     loop
        JAL     0X00
        BEQ     $T0, $T1, loop
        BEQ     $T0, $T1, 0X00
        ADDI    $T0, $T1, 0X01
        LW      $T0, 0X04($T1)
        SW      $T0, 0X04($T1)


# Some Errors
#        add $a4, $0, $0            # UNDEFINED REGISTER
#       subi    $t0, $0, 0x5A02    # UNDEFINED INSTRUCTION
#      j   loop3                   # UNDEFINED LABEL
#      j   LOOP                   # UNDEFINED LABEL
#loop2:  j   loop                    # DUPLICATE LABEL
