# Make a 'binary distribution'
# Yes. It is odd that this uses make and yet Gradle is used to build the Java.

Mite.zip: MiteASM/MiteASM.jar
	cd .. && zip -r Mite.zip Mite -x Mite/MiteASM/build/\* -x Mite/.git/\* -x \*.DS_Store -x Mite/MiteASM/.nb-gradle/\*
	mv ../Mite.zip .

MiteASM/MiteASM.jar:
	cd MiteASM && gradle build
	cp MiteASM/build/libs/MiteASM.jar MiteASM
