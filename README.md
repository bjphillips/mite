# README #

## Mite ##

Mite is the single cycle cut-down MIPS processor described in 
[Digital Design and Computer Architecture](http://textbooks.elsevier.com/web/product_details.aspx?isbn=9780123944245), Second Edition by David Money Harris and Sarah L. Harris (Harris & Harris).
It is a very simple microprocessor intended for educational purposes.

Mite includes:
* SystemVerilog source for the processor, based on the HDL in Harris & Harris
  and using the same signal names as the book.
* SystemVerilog for some of the I/O peripherals available on the 
  [Digilent Basys3](www.digilentinc.com/BASYS3) Xilinx Artix-7 FPGA development boards.
* MiteASM, a simple assembler that generates SystemVerilog instruction memory
  that can be synthesised with the processor.
* Class laboratory instructions for an exercise using Mite on a Basys3 board.

### Previous Versions ###

Version 3 of Mite has been updated for the Basys 3 board, Vivado software and SystemVerilog. Previous versions were compatible with the Basys 2 and Nexys 3 boards, ISE software, and Verilog.

### Getting Started ###

* Download and install [Xilinx Vivado Webpack](https://www.xilinx.com/products/design-tools/vivado/vivado-webpack.html).
* Download the latest release of Mite from [Bitbucket](https://bitbucket.org/bjphillips/mite/downloads) and unzip it somewhere.
* Follow the instructions in the the [Class Instructions](./Docs/ClassInstructions.pdf). This includes documentation of
  the Mite processor and MiteASM in an appendix.

### Code Development ###

MiteASM is distributed as a 'fat' JAR file that should run on any
machine with Java 1.8 or later installed.

If you do wish to modify MiteASM, it can be built and run using Gradle, which
will use the `build.gradle` file to set the appropriate class paths. This means
you do not need to manually configure your environment.

To use the Netbeans IDE, add the [Gradle Support](http://plugins.netbeans.org/plugin/44510/gradle-support) 
plugin to Netbeans.
Then you can open the root project folder as a Netbeans project. The `Build`,
`Debug` and `Run` commands in the IDE work.

### License ###

Mite is distributed under the [BSD License](./LICENSE.md).

### Contacts ###

* Mite was produced by [Braden Phillips](http://www.adelaide.edu.au/directory/braden.phillips) 
