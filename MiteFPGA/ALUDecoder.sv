module ALUDecoder(input logic [5:0] Funct,
    input logic [1:0] ALUOp,
    output logic [2:0] ALUControl);
 
    always_comb
        case (ALUOp)
            2'b00: ALUControl <= 3'b010;  // add (for lw/sw/addi)
            2'b01: ALUControl <= 3'b110;  // sub (for beq)
            default: case(Funct)          // R-type instructions
                6'b100000: ALUControl <= 3'b010; // add
                6'b100010: ALUControl <= 3'b110; // sub
                6'b100100: ALUControl <= 3'b000; // and
                6'b100101: ALUControl <= 3'b001; // or
                6'b101010: ALUControl <= 3'b111; // slt
                default:   ALUControl <= 3'bxxx; // ???
            endcase
        endcase

endmodule
