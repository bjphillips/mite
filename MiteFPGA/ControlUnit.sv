module ControlUnit(input logic [5:0] Op, Funct,
   input logic Zero,
   output logic MemtoReg, MemWrite,
   output logic PCSrc, ALUSrc,
   output logic RegDst, RegWrite,
   output logic Jump,
   output logic [2:0] ALUControl);

   logic [1:0] ALUOp;
   logic Branch;

   MainDecoder MainDecoder_0(Op, MemtoReg, MemWrite, Branch,
      ALUSrc, RegDst, RegWrite, Jump, ALUOp);

   ALUDecoder  ALUDecoder_0(Funct, ALUOp, ALUControl);

   assign PCSrc = Branch & Zero;

endmodule