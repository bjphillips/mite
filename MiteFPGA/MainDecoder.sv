module MainDecoder(input logic [5:0] Op,
    output logic MemtoReg, MemWrite,
    output logic Branch, ALUSrc,
    output logic RegDst, RegWrite,
    output logic Jump,
    output logic [1:0] ALUOp);
 
    logic [8:0] controls;
 
    assign {RegWrite, RegDst, ALUSrc, Branch, MemWrite,
        MemtoReg, Jump, ALUOp} = controls;
 
    always_comb
        case(Op)
            6'b000000: controls <= 9'b110000010; // RTYPE
            6'b100011: controls <= 9'b101001000; // LW
            6'b101011: controls <= 9'b001010000; // SW
            6'b000100: controls <= 9'b000100001; // BEQ
            6'b001000: controls <= 9'b101000000; // ADDI
            6'b000010: controls <= 9'b000000100; // J
            default:   controls <= 9'bxxxxxxxxx; // illegal op
        endcase
       
endmodule
