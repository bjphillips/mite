module InstructionMemory(input logic [5:0] A, output logic [31:0] RD);
   always_comb
      case (A<<2)
         8'h0: RD = 32'h8c080104; // loop : lw $t0 , 0x104 ( $0 ) 
         8'h4: RD = 32'hac080104; // sw $t0 , 0x104 ( $0 ) 
         8'h8: RD = 32'h8c080100; // lw $t0 , 0x100 ( $0 ) 
         8'hc: RD = 32'hac080108; // sw $t0 , 0x108 ( $0 ) 
         8'h10: RD = 32'h8000000; // j loop 
         default: RD = 32'h00000000;
      endcase
endmodule
