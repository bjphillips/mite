module RegisterFile(input logic CLK, 
	input logic WE3, 
    input logic [4:0] RA1, RA2, WA3, 
    input logic [31:0] WD3, 
    output logic [31:0] RD1, RD2);

	logic [31:0] rf[31:0];

  	// three ported register file
  	// read two ports combinationally
  	// write third port on rising edge of CLK
  	// register 0 hardwired to 0

	always_ff @(posedge CLK)
    	if (WE3) rf[WA3] <= WD3;	

  	assign RD1 = (RA1 != 0) ? rf[RA1] : 0;
  	assign RD2 = (RA2 != 0) ? rf[RA2] : 0;
endmodule
