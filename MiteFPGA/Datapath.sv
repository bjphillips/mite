module Datapath(input logic CLK, Reset,
    input logic MemtoReg, PCSrc,
    input logic ALUSrc, RegDst,
    input logic RegWrite, Jump,
    input logic [2:0] ALUControl,
    output logic Zero,
    output logic [31:0] PC,
    input logic [31:0] Instr,
    output logic [31:0] ALUOut, WriteData,
    input logic [31:0] ReadData);
 
    logic [4:0]  WriteReg;
    logic [31:0] PCNext, PCNextBr, PCPlus4, PCBranch;
    logic [31:0] SignImm, SignImmSh;
    logic [31:0] SrcA, SrcB;
    logic [31:0] Result;
 
    // next PC logic
    Flopr #(32) PCreg(CLK, Reset, PCNext, PC);
    Adder PCAdder1(PC, 32'b100, PCPlus4);
    ShiftLeft2 ImmShiftLeft2(SignImm, SignImmSh);
    Adder PCAdder2(PCPlus4, SignImmSh, PCBranch);
    Mux2 #(32) PCBranchMux(PCPlus4, PCBranch, PCSrc, PCNextBr);
    Mux2 #(32) PCMux(PCNextBr, {PCPlus4[31:28], 
        Instr[25:0], 2'b00}, Jump, PCNext);

    // register file logic
    RegisterFile RegisterFile_0(CLK, RegWrite, Instr[25:21], Instr[20:16], 
        WriteReg, Result, SrcA, WriteData);
    Mux2 #(5) WriteMux(Instr[20:16], Instr[15:11],
        RegDst, WriteReg);
    Mux2 #(32) ResultMux(ALUOut, ReadData, MemtoReg, Result);
    SignExtend SignExtend_0(Instr[15:0], SignImm);
 
    // ALU logic
    Mux2 #(32) SrcbMux(WriteData, SignImm, ALUSrc, SrcB);
    ALU ALU(SrcA, SrcB, ALUControl, ALUOut, Zero);

endmodule
