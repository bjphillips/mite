module System(
	input logic CLK, 
	input logic [4:0] btn,	// the 5 buttons on the development board
	input logic [15:0] sw,	// the 16 switches on the development board
	output logic [6:0] seg,	// the 7 segments for the 7 seg display on the development board
	output logic [3:0] an,	// the digit select lines for the 7 seg display
	output logic dp, 		// the decimal point for the 7 seg display
	output logic [15:0] led	// the 16 LEDs on the development board
	);

  	logic [31:0] PC, Instr, WriteData, DataAdr;
  	logic [31:0] ReadData_mem;
	logic [31:0] ReadData;
	logic Reset;
  	logic MemWrite_mem, MemWrite_leds, MemWrite_display;
	logic [7:0] display;
  
 	// instantiate processor and memories
	MIPS MIPS_0(CLK, Reset, PC, Instr, MemWrite, DataAdr, 
    	WriteData, ReadData);
  	InstructionMemory InstructionMemory_0(PC[7:2], Instr);
  	DataMemory DataMemory_0(CLK, MemWrite_mem, DataAdr, WriteData, ReadData_mem);
	
	// use button 4 for Reset
	assign Reset = btn[4];

  	// instantiate some I/O peripherals
	LEDs LEDs_0(CLK, Reset, MemWrite_leds, WriteData[15:0], led);
	Display Display_0(CLK, Reset, MemWrite_display, WriteData[7:0], seg, dp, an);

	// multiplex the data memory and input peripherals
	always_comb
		case (DataAdr) 
			32'h100: ReadData = {28'd0,btn[3:0]};		// 0x100: buttons
			32'h104: ReadData = {16'd0,sw};		// 0x104: switches
			default: ReadData = ReadData_mem;	// else data memory
		endcase
		
  	// address decoding for the output peripherals
  	assign MemWrite_mem = MemWrite & (DataAdr < 32'h100);
  	assign MemWrite_leds = MemWrite & (DataAdr == 32'h104);		// 0x104: leds
	assign MemWrite_display = MemWrite & (DataAdr == 32'h108);	// 0x108: display

endmodule