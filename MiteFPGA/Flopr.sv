module Flopr #(parameter WIDTH = 8) (input CLK, Reset,
	input [WIDTH-1:0] D, 
	output reg [WIDTH-1:0] Q);

  	always @ (posedge CLK, posedge Reset)
    	if (Reset) Q <= 0;
    	else Q <= D;

endmodule

