module DataMemory(input logic CLK, WE,
	input logic [31:0] A, WD,
    output logic [31:0] RD);

  	logic [31:0] RAM[63:0];

	always_comb
		RD <= RAM[A[31:2]]; // word aligned

  	always_ff @ (posedge CLK)
    	if (WE) 
			RAM[A[31:2]] <= WD;

endmodule
