`timescale 10ns / 1ps

// This testbench is for simple integration testing.

module System_tb();

    logic CLK;
    logic [4:0] btn;
    logic [15:0] sw;
    logic [6:0] seg;
    logic [3:0] an;
    logic dp;
    logic [15:0] led;

    // instantiate device to be tested
    System dut(CLK, btn, sw, seg, an, dp, led);
   
    // start and end the test
    initial 
    begin
        btn <= 5'b10000; // Apply a reset
        sw <= 16'h0000;
        #4;
        btn <= 5'b00000; // Release the reset
        #20;
        btn <= 5'b00001;
        sw <= 16'hABCD;
        #20;
    end
 
    // generate clock to sequence tests
    always
    begin
        CLK <= 1; #1; CLK <= 0; #1;
    end

endmodule