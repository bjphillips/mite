`timescale 1ns / 1ps
// I'd normally use some extra levels of hierarchy here/
// In this case I'm going to keep this flat so that there is
// only one module visible in the design hierarchy.
module Display(
	input logic CLK, Reset, WE,
	input logic [7:0] D,
	output logic [6:0] seg,
	output logic dp,
	output logic [3:0] an);
   
	logic [17:0] count;
	logic [6:0] digit3;
	logic [6:0] digit2, digit1, digit0;
	logic [7:0] a;
	logic [3:0] hundreds, tens, ones;
   	logic [3:0] c1,c2,c3,c4,c5,c6,c7;
   	logic [3:0] d1,d2,d3,d4,d5,d6,d7;
	
	// We will just leave the decimal points and digit3 turned off
	assign dp = 1;
	assign digit3 = 7'b111_1111;
	
	// Register the D 
	always_ff @ (posedge CLK)
		if (Reset)
			a <= 8'd0;
		else if (WE)
			a <= D;
	
	// Divide the 100MHz mclk by 2^16 so each display is refreshed for 0.66 ms
	// then use 2 extra bits to alternative between the 4 digits.
	always_ff @ (posedge CLK)
		count <= count + 18'd1;
		
	// Decode the two most significant bits of the counter to select the digits.
	always @ (*)
		case (count[17:16])
			2'b00: {an, seg} = {4'b1110, digit0};
			2'b01: {an, seg} = {4'b1101, digit1};
			2'b10: {an, seg} = {4'b1011, digit2};
			2'b11: {an, seg} = {4'b0111, digit3};
			default:  {an, seg} = {4'b1111, 7'b1111111};
		endcase
		
	// Binary to BCD conversion using obscure magic
   assign d1 = {1'b0,a[7:5]};
   assign d2 = {c1[2:0],a[4]};
   assign d3 = {c2[2:0],a[3]};
   assign d4 = {c3[2:0],a[2]};
   assign d5 = {c4[2:0],a[1]};
   assign d6 = {1'b0,c1[3],c2[3],c3[3]};
   assign d7 = {c6[2:0],c4[3]};
	assign c1 = (d1 >= 4'd5)?(d1+4'd3):d1;
	assign c2 = (d2 >= 4'd5)?(d2+4'd3):d2;
	assign c3 = (d3 >= 4'd5)?(d3+4'd3):d3;
	assign c4 = (d4 >= 4'd5)?(d4+4'd3):d4;
	assign c5 = (d5 >= 4'd5)?(d5+4'd3):d5;
	assign c6 = (d6 >= 4'd5)?(d6+4'd3):d6;
	assign c7 = (d7 >= 4'd5)?(d7+4'd3):d7;
   assign ones = {c5[2:0],a[0]};
   assign tens = {c7[2:0],c5[3]};
   assign hundreds = {1'b0, 1'b0, c6[3],c7[3]};

	// Seven segment decoders
	always_comb
		case (hundreds) // gfe_dcba active low
			0: digit2 = 7'b100_0000;
			1: digit2 = 7'b111_1001;
			2: digit2 = 7'b010_0100;
			3: digit2 = 7'b011_0000;
			4: digit2 = 7'b001_1001;
			5: digit2 = 7'b001_0010;
			6: digit2 = 7'b000_0011;
			7: digit2 = 7'b111_1000;
			8: digit2 = 7'b000_0000;
			9: digit2 = 7'b001_1000;
			default: digit2 = 7'b000_0110;
		endcase
	always_comb
		case (tens) // gfe_dcba active low
			0: digit1 = 7'b100_0000;
			1: digit1 = 7'b111_1001;
			2: digit1 = 7'b010_0100;
			3: digit1 = 7'b011_0000;
			4: digit1 = 7'b001_1001;
			5: digit1 = 7'b001_0010;
			6: digit1 = 7'b000_0011;
			7: digit1 = 7'b111_1000;
			8: digit1 = 7'b000_0000;
			9: digit1 = 7'b001_1000;
			default: digit1 = 7'b000_0110;
		endcase
	always_comb
		case (ones) // gfe_dcba active low
			0: digit0 = 7'b100_0000;
			1: digit0 = 7'b111_1001;
			2: digit0 = 7'b010_0100;
			3: digit0 = 7'b011_0000;
			4: digit0 = 7'b001_1001;
			5: digit0 = 7'b001_0010;
			6: digit0 = 7'b000_0011;
			7: digit0 = 7'b111_1000;
			8: digit0 = 7'b000_0000;
			9: digit0 = 7'b001_1000;
			default: digit0 = 7'b000_0110;
		endcase

endmodule
