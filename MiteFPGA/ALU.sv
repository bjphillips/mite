module ALU(input logic [31:0] SrcA, SrcB,
    input logic [2:0] ALUControl,
    output logic [31:0] ALUResult,
    output logic Zero);
 
    logic [31:0] CondInvB, Sum;
 
    assign CondInvB = ALUControl[2] ? ~SrcB : SrcB;
    assign Sum = SrcA + CondInvB + ALUControl[2];
 
    always_comb
        case (ALUControl[1:0])
            2'b00: ALUResult = SrcA & SrcB;
            2'b01: ALUResult = SrcA | SrcB;
            2'b10: ALUResult = Sum;
            2'b11: ALUResult = Sum[31];
        endcase

    assign Zero = (ALUResult == 32'b0);

endmodule
