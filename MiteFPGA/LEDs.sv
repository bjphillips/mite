module LEDs(input logic CLK, Reset, WE,
   input logic [15:0] Data,
	output logic [15:0] led);

	always @ (posedge CLK)
		if (Reset)
			led <= 16'd0;
		else if (WE)
			led <= Data;
			
endmodule
