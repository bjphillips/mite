module MIPS(input logic CLK, Reset,
   output logic [31:0] PC,
   input logic [31:0] Instr,
   output logic MemWrite,
   output logic [31:0] ALUOut, WriteData,
   input logic [31:0] ReadData);

   logic MemtoReg, ALUSrc, RegDst, 
      RegWrite, Jump, PCSrc, Zero;
  
   logic [2:0] alucontrol;

   ControlUnit ControlUnit_0(Instr[31:26], Instr[5:0], Zero,
      MemtoReg, MemWrite, PCSrc,
      ALUSrc, RegDst, RegWrite, Jump,
      alucontrol);
  
   Datapath Datapath_0(CLK, Reset, MemtoReg, PCSrc,
      ALUSrc, RegDst, RegWrite, Jump,
      alucontrol,
      Zero, PC, Instr,
      ALUOut, WriteData, ReadData);

endmodule

